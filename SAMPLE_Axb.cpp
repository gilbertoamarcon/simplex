#include <iostream>
#include <iomanip>
#include <list>
#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include "sistemaLinear.hpp"

using namespace std;

int main(int argc, char **argv){

	int M = 2; 
	int N = 2;
	double** A;
	double* b;
	double* x;
	
	A = (double**)calloc(M, sizeof(double*));
	for( int i = 0; i < M; i++)
		A[i] = (double*)calloc(N, sizeof(double));
	x = (double*)calloc(N,sizeof(double));
	b = (double*)calloc(M,sizeof(double));

	A[0][0] = 1;
	A[0][1] = 2;
	A[1][0] = 1;
	A[1][1] = 3;

	b[0] = 5;
	b[1] = 7;

	SistemaLinear sistemaLinear;

	sistemaLinear.resolve(A,x,b,M,0);

	cout << "Matriz A:" << endl;
	imprimeMatriz(A,M,M);

	cout << "Vetor x:";
	imprimeVetor(x,N);

	cout << "Vetor b:";
	imprimeVetor(b,M);

	for(int i = 0; i < M; i++)
		free(A[i]);
	free(A);
	free(x);
	free(b);

	return 0;
}