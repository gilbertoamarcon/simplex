#include <iostream>
#include <iomanip>
#include <list>
#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include "sistemaLinear.hpp"

using namespace std;

int main(int argc, char **argv){

	int n = 5;

	double* x = (double*)calloc(n,sizeof(double));
	double* y = (double*)calloc(n,sizeof(double));
	double* z = (double*)calloc(n,sizeof(double));
	double* p = (double*)calloc(4,sizeof(double));

	x[0] = 1;
	y[0] = 0;
	z[0] = 0;

	x[1] = 0;
	y[1] = 1;
	z[1] = 0;

	x[2] = 0;
	y[2] = 0;
	z[2] = 1;

	x[3] = 0;
	y[3] = 0;
	z[3] = 1;

	x[4] = 0;
	y[4] = 0;
	z[4] = 1;	

	fitPlaneL2(x,y,z,n,p);

	cout << "a:" << p[0] << endl;
	cout << "b:" << p[1] << endl;
	cout << "c:" << p[2] << endl;
	cout << "d:" << p[3] << endl;

	return 0;
}