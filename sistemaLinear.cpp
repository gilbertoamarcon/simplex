#include "sistemaLinear.hpp"

SistemaLinear::SistemaLinear(){}

// ----------------------------------------------------------------
// Método público para solução de SL Ax=b
// Entradas: A, x, b, ordem (ordem de A),
// ----------------------------------------------------------------
void SistemaLinear::resolve(double** A,double* x,double* b,int ordem,int t){
    o = ordem;
    L = inicializaMatriz(o,o);
    U = inicializaMatriz(o,o);
    M = inicializaMatriz(o,o+1); 
    preparaMatriz(A,b,t);
    fatoracaoLU();
    for(int i = 0; i < o; i++)
        x[i] = M[i][o];
    preparaMatriz(L,x,0);
    retrosubstituicaoL(x);
    preparaMatriz(U,x,0);
    retrosubstituicaoU(x);
    for(int i = 0; i < o; i++)
        free(L[i]);
    free(L);
    for(int i = 0; i < o; i++)
        free(U[i]);
    free(U);
    for(int i = 0; i < o; i++)
        free(M[i]);
    free(M);
}

// Aloca espaco de memória para a matriz
double** SistemaLinear::inicializaMatriz(int m,int n){
    double** M;
    M = (double**)calloc(m, sizeof(double*));
    for(int i = 0; i < m; i++){
    	M[i] = (double*)calloc(n, sizeof(double));
        for(int j = 0; j < n; j++)
            M[i][j] = 0;
    }
    return M;
}

// Copia o conteúdo da matriz A e do vetor b para a matriz M
void SistemaLinear::preparaMatriz(double** H,double* v,int t){
    for (int i = 0; i < o; i++){
        for (int j = 0; j < o; j++)
            if(t)
                M[i][j] = H[j][i];
            else
                M[i][j] = H[i][j];
        M[i][o] = v[i];
    }
}

void SistemaLinear::retrosubstituicaoU(double* x){

    // Este loop Sobe a matriz escalonada achando o valor de cada variável
    for (int i = o-1; i >= 0; i--){

        // Preenche o vetor x com b
        x[i] = M[i][o];

        // Subtrai de x todos os valores ponderados entre b e o pivô
        for (int j = (o-1); j > i; j--)
            x[i] -= M[i][j]*x[j];

        // Divide x pelo pivô
        x[i] /= M[i][i];
    }

}

void SistemaLinear::retrosubstituicaoL(double* x){

    // Este loop Sobe a matriz escalonada achando o valor de cada variável
    for (int i = 0; i < o; i++){

        // Preenche o vetor x com b
        x[i] = M[i][o];

        // Subtrai de x todos os valores ponderados entre b e o pivô
        for (int j = 0; j < i; j++)
            x[i] -= M[i][j]*x[j];

        // Divide x pelo pivô
        x[i] /= M[i][i];
    }

}

void SistemaLinear::fatoracaoLU(){

    // Zera os elementos de L e U triangularizando-as. Preenche com 1 os elementos da diagonal de L
    for (int i = 0; i < o; i++){
        for (int j = 0; j < o; j++){
            if( i > j)
                U[i][j] = 0;
            else if(i < j)
                L[i][j] = 0;
            else
                L[i][j] = 1;
        }
    }

    // Checagem e permutação de linhas
    for (int i = 0; i < o; i++){

        // Detecta pivot maximo
        int maxPivot = i;
        for(int j = i+1; j < o; j++)
            if(abs(M[j][i]) > abs(M[i][i]))
                maxPivot = j;

        // Troca linhas
        if(maxPivot != i){
            for(int j = 0; j <= o; j++){
                double flaux     = M[i][j];
                M[i][j]         = M[maxPivot][j];
                M[maxPivot][j]  = flaux;
            }
        }  
    }

    double soma = 0; // Variável auxiliar que armazena os valores a serem subtraídos a cada passo
    
    for (int i = 0; i < o; i++){

        // A cada passada deste laço, define-se uma linha de U
        for (int j = i; j < o; j++){
            soma = 0;
            for (int k = 0; k < i; k++)
                soma += L[i][k]*U[k][j];
            U[i][j] = M[i][j] - soma;
        }

        // A cada passada deste laço, define-se uma coluna de L
        for (int j = i+1; j < o; j++){
            soma = 0;
            for (int k = 0; k < j; k++)
                soma += L[j][k]*U[k][i];
            L[j][i] = (M[j][i] - soma)/U[i][i];
        }

    }
}

void fitPlaneL2(double* ix,double* iy,double* iz,int n,double* p){

    int M = n; 
    int N = 3;

    double** A;     // mxn
    double** At;    // nxm
    double** AtA;   // nxn
    double** AtAi;  // nxn
    double** Ap;    // nxm
    double* x;      // nx1
    double* b;      // mx1
    
    A = (double**)calloc(M, sizeof(double*));
    for(int i = 0; i < M; i++)
        A[i] = (double*)calloc(N, sizeof(double));
    
    At = (double**)calloc(N, sizeof(double*));
    for(int i = 0; i < N; i++)
        At[i] = (double*)calloc(M, sizeof(double));
    
    AtA = (double**)calloc(N, sizeof(double*));
    for(int i = 0; i < N; i++)
        AtA[i] = (double*)calloc(N, sizeof(double));
    
    AtAi = (double**)calloc(N, sizeof(double*));
    for(int i = 0; i < N; i++)
        AtAi[i] = (double*)calloc(N, sizeof(double));
    
    Ap = (double**)calloc(N, sizeof(double*));
    for(int i = 0; i < N; i++)
        Ap[i] = (double*)calloc(M, sizeof(double));

    x = (double*)calloc(N,sizeof(double));
    b = (double*)calloc(M,sizeof(double));

    for(int i = 0; i < M; i++){
        A[i][0] = ix[i];
        A[i][1] = iy[i];
        A[i][2] = 1;
        b[i]    = iz[i];
    }

    transposta(A,At,M,N);
    mutiplica(At,A,AtA,N,N,M);
    inversa(AtA,AtAi,N);
    mutiplica(AtAi,At,Ap,N,M,N);
    mutiplica(Ap,b,x,N,M);

    p[0] =  x[0]/x[2];
    p[1] =  x[1]/x[2];
    p[2] = -1/x[2];
    p[3] =  1;

    double m = sqrt(pow(p[0],2)+pow(p[1],2)+pow(p[2],2));

    p[0] /= m;
    p[1] /= m;
    p[2] /= m;
    p[3] /= m;

    for(int i = 0; i < M; i++)
        free(A[i]);
    free(A);

    for(int i = 0; i < N; i++)
        free(At[i]);
    free(At);

    for(int i = 0; i < N; i++)
        free(AtA[i]);
    free(AtA);

    for(int i = 0; i < N; i++)
        free(AtAi[i]);
    free(AtAi);

    for(int i = 0; i < N; i++)
        free(Ap[i]);
    free(Ap);

    free(x);
    free(b);

}

void imprimeMatriz(double** mat,int m,int n){
    cout << fixed;
    for(int i = 0; i < m; i++){
        for(int j = 0; j < n; j++)
            cout << setprecision(3) << setw(10) << mat[i][j];
        cout << endl;
    }
}

void imprimeVetor(double* v,int m){
    cout << fixed;
    for(int i = 0; i < m; i++)
        cout << setprecision(3) << setw(10) << v[i];
    cout << endl;
}

// Atribuir valores de Inxn em Anxn
void identidade(double** A,int n){
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            if(i == j)
                A[i][j] = 1;
            else
                A[i][j] = 0;
}

// Inicializa vetor como e = [1 1 1 1 ...]
void vetorE(double* e,int n){
    for (int i = 0; i < n; i++)
        e[i] = 1;
}

// Faz a transposta de Gmxn em Tnxm
void transposta(double** G,double** T,int m,int n){
    for (int i = 0; i < m; i++)
        for (int j = 0; j < n; j++)
            T[j][i] = G[i][j];
}

// Encontra a inversa de Anxn e atribui a Bnxn
void inversa(double** A,double** B,int n){
    double* x = (double*)calloc(n,sizeof(double));
    double* b = (double*)calloc(n,sizeof(double));
    for(int i = 0; i < n; i++){
        for(int j = 0; j < n; j++)
            if(j == i)
                b[j] = 1;
            else
                b[j] = 0;
        SistemaLinear sistemaLinear;
        sistemaLinear.resolve(A,x,b,n,0);
        for(int j = 0; j < n; j++)
            B[j][i] = x[j];
    }
    free(b);
    free(x);
}

// Determinante de Anxn
double determinante(double** A,int n){
	int l;
	double det = 0;
	if(n == 1) 
		return A[0][0];
	if(n == 2) 
		return A[0][0] * A[1][1] - A[1][0] * A[0][1];
	det = 0;
	for(int k = 0; k < n; k++){
		double** M = (double**)calloc(n-1,sizeof(double*));
		for(int i = 0; i < n-1; i++)
			M[i] = (double*)calloc(n-1,sizeof(double));
		for(int i = 1; i < n; i++){
			l = 0;
			for(int j = 0; j < n; j++){
				if(j == k)
					continue;
				M[i-1][l] = A[i][j];
				l++;
			}
		}
		det += pow(-1,k+2)*A[0][k]*determinante(M,n-1);
		for(int i = 0; i < n-1; i++)
			free(M[i]);
		free(M);
	}
   return(det);
}

// Soma Amxn a Bmxn multiplicado por a e atribui a Cmxn
void soma(double** A,double** B,double** C,double a,int m,int n){
    for (int i = 0; i < m; i++)
        for (int j = 0; j < n; j++)
            C[i][j] = A[i][j] + a*B[i][j];
}

// Soma Am a Bm multiplicado por a e atribui a Cm
void soma(double* A,double* B,double* C,double a,int m){
    for (int i = 0; i < m; i++)
        C[i] = A[i] + a*B[i];
}

// Multiplica Gmxo por Toxn e atribui a Amxn
void mutiplica(double** G,double** T,double** A,int m,int n,int o){
    for (int i = 0; i < m; i++)
        for (int j = 0; j < n; j++){
            A[i][j] = 0;
            for (int k = 0; k < o; k++)
                A[i][j] += G[i][k] * T[k][j];
        }
}

// Multiplica Tmxn por Vn e atribui a Bm
void mutiplica(double** T,double* V,double* B,int m,int n){
    for(int i = 0; i < m; i++){
        B[i] = 0;
        for(int j = 0; j < n; j++)
            B[i] += T[i][j]*V[j];
    }
}

// Multiplica Am por a e atribui a Bm
void mutiplica(double* A,double* B,double a,int m){
    for(int i = 0; i < m; i++)
        B[i] = a*A[i];
}

// Multiplica Am por Bm e retorna resultado
double produtoEscalar(double* A,double* B,int m){
    double aux = 0;
    for(int i = 0; i < m; i++)
        aux += B[i]*A[i];
    return aux;
}

// Retorna o módulo do vetor v
double modulo(double* v,int n){
    double aux = 0;
    for(int i = 0; i < n; i++)
        aux += pow(v[i],2);
    return sqrt(aux);
}

// Retorna 1 se v == 0, e 0 caso contrário
int igualZero(double* v,int n, double tolerancia){
    for(int i = 0; i < n; i++)
        if(abs(v[i]) > tolerancia) return 0;
    return 1;
}

// Retorna 1 se v > 0, e 0 caso contrário
int maiorQueZero(double* v,int n){
    for(int i = 0; i < n; i++)
        if(v[i] <= 0) return 0;
    return 1;
}