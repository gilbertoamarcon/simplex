#ifndef __SISTEMALINEAR_HPP__
#define __SISTEMALINEAR_HPP__
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <cmath>
using namespace std;

class SistemaLinear{
    private:
        int o;
        double** L;
        double** U;
        double** M;
        double** inicializaMatriz(int m,int n);
        void preparaMatriz(double** H,double* v,int t);
        void retrosubstituicaoU(double* x);
        void retrosubstituicaoL(double* x);
        void fatoracaoLU();
    public:
        SistemaLinear();
        void resolve(double** A,double* x,double* b,int ordem,int t);
};

// Encontra o plano aproximante ao conjunto de pontos (x,y,z) por regressao linear
// n indica a quantidade de amostras
// p recebe o plano aproximante: a,b,c,d = p[0],p[1],p[2],p[3], com normal unitaria
void fitPlaneL2(double* ix,double* iy,double* iz,int n,double* p);

void imprimeMatriz(double** mat,int m,int n);

void imprimeVetor(double* v,int m);

// Atribuir valores de Inxn (identidade) em Anxn
void identidade(double** A,int n);

// Inicializa vetor como e = [1 1 1 1 ...]
void vetorE(double* e,int n);

// Faz a transposta de Gmxn em Tnxm
void transposta(double** G,double** T,int m,int n);

// Encontra a inversa de Anxn e atribui a Bnxn
void inversa(double** A,double** B,int n);

// Determinante de Anxn
double determinante(double** A,int n);

// Soma Amxn a Bmxn multiplicado por a e atribui a Cmxn
void soma(double** A,double** B,double** C,double a,int m,int n);

// Soma Am a Bm multiplicado por a e atribui a Cm
void soma(double* A,double* B,double* C,double a,int m);

// Multiplica Gmxo por Toxn e atribui a Amxn
void mutiplica(double** G,double** T,double** A,int m,int n,int o);

// Multiplica Tmxn por Vn e atribui a Bm
void mutiplica(double** T,double* V,double* B,int m,int n);

// Multiplica Am por a e atribui a Bm
void mutiplica(double* A,double* B,double a,int m);

// Multiplica Am por Bm e retorna resultado
double produtoEscalar(double* A,double* B,int m);

// Retorna o módulo do vetor v
double modulo(double* v,int n);

// Retorna 1 se v == 0, e 0 caso contrário
int igualZero(double* v,int n, double tolerancia);

// Retorna 1 se v > 0, e 0 caso contrário
int maiorQueZero(double* v,int n);

#endif