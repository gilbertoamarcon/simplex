#include <iostream>
#include <iomanip>
#include <list>
#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include "sistemaLinear.hpp"
#define BUFFER_SIZE 	256
using namespace std;

void imprimeProblema(int M,int N,double** A,double* b,double* c);

void imprimeA(list<int> *listaN,list<int> *listaB,double** B,double* x,double* y,double* s,int M,int N);

void imprimeLista(list<int> *lista);

void imprimeQuadroGH(int numInt,double** mat,int* vB,int* vN,int m,int n);

int verificaTamDados(int* M,int* N,char **argv);

int carregaSimplex(int M,int N,double** A,double* b,double* c,list<int> *listaN,list<int> *listaB,char **argv);

int carregaMgrande(int* M,int* N,double** A,double* b,double* c,double* x,char **argv);

int carregaPtsInteriores(int M,int N,double** A,double* b,double* c,double* x,char **argv);

int carregaPrimalDual(int M,int N,double** A,double* b,double* c,double* x,double* s,double* y,char **argv);

int simplexPrimal(int M,int N,double** A,double* b,double* c,double* x,list<int> *listaN,list<int> *listaB);

int simplexDual(int M,int N,double** A,double* b,double* c,double* x,list<int> *listaN,list<int> *listaB);

int garfinkelNemhauser(int M,int N,double** A,double* b,double* c,double* x,list<int> *listaN,list<int> *listaB);

int afimEscala(int M,int N,double** A,double* b,double* c,double* x);

int karmarkar(int M,int N,double** A,double* b,double* c,double* x);

int primalDual(int M,int N,double** A,double* b,double* c,double* x,double* s,double* y);

int mensagemSol(double* x,double z,int N);

int mensagemSolPrimalDual(double* x,double* s,double z,int N);

int mensagemArquivoInexistente();

int mensagemArgumentos();

int mensagemAjuda();

int main(int argc, char **argv){

	if(argc < 3)
		return mensagemArgumentos();

	int M = -1; 
	int N = 0;
	double** A;
	double* b;
	double* c;
	double* x;
	double* s;
	double* y;
	list<int> listaN;
	list<int> listaB;
	int arquivoInexistente = 0;
	
	// AlocaÃ§Ã£o de memÃ³ria
	verificaTamDados(&M,&N,argv);

	// Se M grande
	if(argv[2][0] == 'm'){
		N++;
		M++;
	}else if(argv[2][0] == 'b')
		M-=2;

	A = (double**)calloc(M, sizeof(double*));
	for( int i = 0; i < M; i++)
		A[i] = (double*)calloc(N, sizeof(double));
	b = (double*)calloc(M,sizeof(double));
	y = (double*)calloc(M,sizeof(double));
	c = (double*)calloc(N,sizeof(double));
	x = (double*)calloc(N,sizeof(double));
	s = (double*)calloc(N,sizeof(double));

	switch(argv[2][0]){
		case 'p':
			if(arquivoInexistente = carregaSimplex(M,N,A,b,c,&listaN,&listaB,argv))
				break;
			imprimeProblema(M,N,A,b,c);
			simplexPrimal(M,N,A,b,c,x,&listaN,&listaB);
			break;
		case 'd':
			if(arquivoInexistente = carregaSimplex(M,N,A,b,c,&listaN,&listaB,argv))
				break;
			imprimeProblema(M,N,A,b,c);
			simplexDual(M,N,A,b,c,x,&listaN,&listaB);
			break;
		case 'g':
			if(arquivoInexistente = carregaSimplex(M,N,A,b,c,&listaN,&listaB,argv))
				break;
			imprimeProblema(M,N,A,b,c);
			garfinkelNemhauser(M,N,A,b,c,x,&listaN,&listaB);
			break;
		case 'a':
			if(arquivoInexistente = carregaPtsInteriores(M,N,A,b,c,x,argv))
				break;
			imprimeProblema(M,N,A,b,c);
			afimEscala(M,N,A,b,c,x);
			break;
		case 'm':
			if(arquivoInexistente = carregaMgrande(&M,&N,A,b,c,x,argv))
				break;
			imprimeProblema(M,N,A,b,c);
			afimEscala(M,N,A,b,c,x);
			break;
		case 'k':
			if(arquivoInexistente = carregaPtsInteriores(M,N,A,b,c,x,argv))
				break;
			imprimeProblema(M,N,A,b,c);
			karmarkar(M,N,A,b,c,x);
			break;
		case 'b':
			if(arquivoInexistente = carregaPrimalDual(M,N,A,b,c,x,s,y,argv))
				break;
			imprimeProblema(M,N,A,b,c);
			primalDual(M,N,A,b,c,x,s,y);
			break;
		default:
			mensagemArgumentos();
			break;
	}

	if(arquivoInexistente)
		mensagemArquivoInexistente();

	for(int i = 0; i < M; i++)
		free(A[i]);
	free(A);
	free(b);
	free(c);
	free(x);
	free(s);

	return 0;
}

int mensagemSol(double* x,double z,int N){
	cout << "=======================================================================" << endl;
	cout << "Sol otima: z = " << z << endl;
	cout << "Vetor x:";
	imprimeVetor(x,N);
	cout << "=======================================================================" << endl;
	return 0;
}

int mensagemSolPrimalDual(double* x,double* s,double z,int N){
	cout << "=======================================================================" << endl;
	cout << "Sol otima: z = " << z << endl;
	cout << "Vetor x:";
	imprimeVetor(x,N);
	cout << "Vetor s:";
	imprimeVetor(s,N);
	cout << "=======================================================================" << endl;
	return 0;
}

int mensagemArquivoInexistente(){
	cout << "||===================================================================||" << endl;
	cout << "|| Erro: Arquivo inexistente                                         ||" << endl;
	cout << "||===================================================================||" << endl;
	return 0;
}

int mensagemArgumentos(){
	cout << "||===================================================================||" << endl;
	cout << "|| Erro: Faltam argumentos / argumentos invalidos.                   ||" << endl;
	cout << "||===================================================================||" << endl;
	cout << endl;
	return mensagemAjuda();
}

int mensagemAjuda(){
	cout << "||===================================================================||" << endl;
	cout << "|| Formato correto de chamada:                                       ||" << endl;
	cout << "|| ./main <arquivo com def do problema> <algoritmo desejado>         ||" << endl;
	cout << "|| Simplex Primal:        p                                          ||" << endl;
	cout << "|| Simplex Dual:          d                                          ||" << endl;
	cout << "|| Garfinkel-Nemhauser:   g                                          ||" << endl;
	cout << "|| Afim Escala:           a                                          ||" << endl;
	cout << "|| Karmarkar:             k                                          ||" << endl;
	cout << "|| M grande:              m                                          ||" << endl;
	cout << "|| Primal Dual:           b                                          ||" << endl;
	cout << "||===================================================================||" << endl;
	cout << endl;
	cout << "||===================================================================||" << endl;
	cout << "|| Formato para arquivo de def do problema Simplex:                  ||" << endl;
	cout << "|| a11 a12 a13 a14 a15 ... a1n b1                                    ||" << endl;
	cout << "|| a21 a22 a23 a24 a25 ... a2n b2                                    ||" << endl;
	cout << "|| a31 a32 a33 a34 a35 ... a3n b3                                    ||" << endl;
	cout << "||          ...                                                      ||" << endl;
	cout << "|| am1 am2 am3 am4 am5 ... amn bm                                    ||" << endl;
	cout << "|| Ib1 Ib2 Ib3 Ib4 ... Ibm                                           ||" << endl;
	cout << "||===================================================================||" << endl;
	cout << endl;
	cout << "||===================================================================||" << endl;
	cout << "|| Formato para arquivo de def do problema de pontos interiores:     ||" << endl;
	cout << "||  c1  c2  c3  c4  c5 ...  cn                                       ||" << endl;
	cout << "|| a11 a12 a13 a14 a15 ... a1n b1                                    ||" << endl;
	cout << "|| a21 a22 a23 a24 a25 ... a2n b2                                    ||" << endl;
	cout << "|| a31 a32 a33 a34 a35 ... a3n b3                                    ||" << endl;
	cout << "||          ...                                                      ||" << endl;
	cout << "|| am1 am2 am3 am4 am5 ... amn bm                                    ||" << endl;
	cout << "|| x01 x02 x03 x04 x05 ... x0n                                       ||" << endl;
	cout << "||===================================================================||" << endl;
	return 0;
};

void imprimeProblema(int M,int N,double** A,double* b,double* c){
	cout << "=======================================================================" << endl;
	cout << "Def do problema:" << endl;
	cout << "Matriz A:" << endl;
	imprimeMatriz(A,M,N);
	cout << "Vetor b:";
	imprimeVetor(b,M);
	cout << "Vetor c:";
	imprimeVetor(c,N);
	cout << "=======================================================================" << endl;
}

void imprimeA(list<int> *listaN,list<int> *listaB,double** B,double* x,double* y,double* s,int M,int N){
	cout << "Lista N: ";
	imprimeLista(listaN);
	cout << endl;
	cout << "Lista B: ";
	imprimeLista(listaB);
	cout << endl;
	cout << "Matriz B:" << endl;
	imprimeMatriz(B,M,M);
	cout << "Vetor x:";
	imprimeVetor(x,N);
	cout << "Vetor y:";
	imprimeVetor(y,M);
	cout << "Vetor s:";
	imprimeVetor(s,N);
}

void imprimeLista(list<int> *lista){
	for(list<int>::iterator i = lista->begin(); i != lista->end(); i++)
		cout << *i+1 << "; ";
}

void imprimeQuadroGH(int numInt,double** mat,int* vB,int* vN,int m,int n){
	cout << fixed;
	char aux[255];
	sprintf(aux,"Q%i",numInt);
	cout << setw(10) << aux << setw(10) << "RHS";
	for(int j = 0; j < n; j++)
		cout << setw(10) << vN[j];
	cout << endl;
	for(int i = 0; i < m+1; i++){
		if(i==0)
			cout << setprecision(3) << setw(10) << "B";
		else
			cout << setprecision(3) << setw(10) << vB[i-1];
		for(int j = 0; j < n+1; j++)
			cout << setprecision(3) << setw(10) << mat[i][j];
		cout << endl;
	}
}

int verificaTamDados(int* M,int* N,char **argv){
	// Abre e checa existÃªncia do arquivo
	FILE *fileStream = fopen(argv[1], "r");
	if (fileStream == NULL)
		return 1;
	// Calcula tamanho dos dados
	char charAux = 0;
	int intAux = 0; 
	char fileBuffer[BUFFER_SIZE];
	while(fgets(fileBuffer,BUFFER_SIZE,fileStream) != NULL){
		if((*M)++ < 0){
			intAux = 0;
			while(true){
				while(fileBuffer[intAux] == ' ')
					intAux++;
				if(fileBuffer[intAux] == '\0' | fileBuffer[intAux] == '\n' | intAux >= BUFFER_SIZE)
					break;
				(*N)++;
				while(fileBuffer[intAux] != ' ' && fileBuffer[intAux] != '\0'  && fileBuffer[intAux] != '\n' && intAux < BUFFER_SIZE)
					intAux++;
			}
		}
	}
	(*M)-=1;
	fclose(fileStream);
	return 0;
}

int carregaSimplex(int M,int N,double** A,double* b,double* c,list<int> *listaN,list<int> *listaB,char **argv){
	int m = -1; 
	int n = 0; 
	for(int i = 0; i < N; i++)
		listaN->push_back(i);
	// Parsing
	FILE *fileStream = fopen(argv[1], "r");
	if (fileStream == NULL)
		return 1;
	char fileBuffer[BUFFER_SIZE];
	int intAux = 0; 
	while(fgets(fileBuffer,BUFFER_SIZE,fileStream) != NULL){
		intAux = 0; 
		while(true){
			while(fileBuffer[intAux] == ' ')
				intAux++;
			if(fileBuffer[intAux] == '\0' | fileBuffer[intAux] == '\n' | intAux >= BUFFER_SIZE){
				n = 0;
				m++;
				break;
			}
			if(m < 0)
				c[n++] = atof(fileBuffer+intAux);
			else if(m == M){
				listaB->push_back(atoi(fileBuffer+intAux)-1);
				listaN->remove(atoi(fileBuffer+intAux)-1);
			}
			else{
				if(n == N)
					b[m] = atof(fileBuffer+intAux);
				else
					A[m][n++] = atof(fileBuffer+intAux);				
			}
			while(fileBuffer[intAux] != ' ' && fileBuffer[intAux] != '\0'  && fileBuffer[intAux] != '\n' && intAux < BUFFER_SIZE)
				intAux++;
		}
	}
	fclose(fileStream);
	return 0;
}

int carregaMgrande(int* M,int* N,double** A,double* b,double* c,double* x,char **argv){
	int m = -1; 
	int n = 0; 
	// Parsing
	FILE *fileStream = fopen(argv[1], "r");
	if (fileStream == NULL)
		return 1;
	char fileBuffer[BUFFER_SIZE];
	int intAux = 0;
	double acc = 0;
	while(fgets(fileBuffer,BUFFER_SIZE,fileStream) != NULL){
		intAux = 0;
		acc = 0;
		while(true){
			while(fileBuffer[intAux] == ' ')
				intAux++;
			if(fileBuffer[intAux] == '\0' | fileBuffer[intAux] == '\n' | intAux >= BUFFER_SIZE){
				if(m >= 0 && m < (*N)-1)
					A[m][n] = b[m]-acc;
				n = 0;
				m++;
				break;
			}
			if(m < 0)
				c[n++] = atof(fileBuffer+intAux);
			else{
				if(n == (*N)-1)
					b[m] = atof(fileBuffer+intAux);
				else{
					A[m][n] = atof(fileBuffer+intAux);
					acc += A[m][n];
					n++;			
				}
			}
			while(fileBuffer[intAux] != ' ' && fileBuffer[intAux] != '\0'  && fileBuffer[intAux] != '\n' && intAux < BUFFER_SIZE)
				intAux++;
		}
	}
	c[(*N)-1] = 1e4;
	for(int i = 0; i < (*N); i++)
		x[i] = 1;
	fclose(fileStream);
	return 0;
}

int carregaPtsInteriores(int M,int N,double** A,double* b,double* c,double* x,char **argv){
	int m = -1; 
	int n = 0; 
	// Parsing
	FILE *fileStream = fopen(argv[1], "r");
	if (fileStream == NULL)
		return 1;
	char fileBuffer[BUFFER_SIZE];
	int intAux = 0; 
	while(fgets(fileBuffer,BUFFER_SIZE,fileStream) != NULL){
		intAux = 0; 
		while(true){
			while(fileBuffer[intAux] == ' ')
				intAux++;
			if(fileBuffer[intAux] == '\0' | fileBuffer[intAux] == '\n' | intAux >= BUFFER_SIZE){
				n = 0;
				m++;
				break;
			}
			if(m < 0)
				c[n++] = atof(fileBuffer+intAux);
			else if(m == M)
				x[n++] = atof(fileBuffer+intAux);
			else{
				if(n == N)
					b[m] = atof(fileBuffer+intAux);
				else
					A[m][n++] = atof(fileBuffer+intAux);				
			}
			while(fileBuffer[intAux] != ' ' && fileBuffer[intAux] != '\0'  && fileBuffer[intAux] != '\n' && intAux < BUFFER_SIZE)
				intAux++;
		}
	}
	fclose(fileStream);
	return 0;
}

int carregaPrimalDual(int M,int N,double** A,double* b,double* c,double* x,double* s,double* y,char **argv){
	int m = -1; 
	int n = 0; 
	// Parsing
	FILE *fileStream = fopen(argv[1], "r");
	if (fileStream == NULL)
		return 1;
	char fileBuffer[BUFFER_SIZE];
	int intAux = 0; 
	while(fgets(fileBuffer,BUFFER_SIZE,fileStream) != NULL){
		intAux = 0; 
		while(true){
			while(fileBuffer[intAux] == ' ')
				intAux++;
			if(fileBuffer[intAux] == '\0' | fileBuffer[intAux] == '\n' | intAux >= BUFFER_SIZE){
				n = 0;
				m++;
				break;
			}
			if(m < 0)
				c[n++] = atof(fileBuffer+intAux);
			else if(m == M)
				x[n++] = atof(fileBuffer+intAux);
			else if(m == M+1)
				s[n++] = atof(fileBuffer+intAux);
			else if(m == M+2)
				y[n++] = atof(fileBuffer+intAux);
			else{
				if(n == N)
					b[m] = atof(fileBuffer+intAux);
				else
					A[m][n++] = atof(fileBuffer+intAux);				
			}
			while(fileBuffer[intAux] != ' ' && fileBuffer[intAux] != '\0'  && fileBuffer[intAux] != '\n' && intAux < BUFFER_SIZE)
				intAux++;
		}
	}
	return 0;
}

int simplexPrimal(int M,int N,double** A,double* b,double* c,double* x,list<int> *listaN,list<int> *listaB){

	cout << "||===================================================================||" << endl;
	cout << "|| Algoritmo Simplex Primal                                          ||" << endl;
	cout << "||===================================================================||" << endl;

	double* p = (double*)calloc(M,sizeof(double));
	double* q = (double*)calloc(M,sizeof(double));
	double* d = (double*)calloc(M,sizeof(double));
	double* s = (double*)calloc(N,sizeof(double));
	double* y = (double*)calloc(M,sizeof(double));
	double* a = (double*)calloc(M,sizeof(double));
	double** B = (double**)calloc(M, sizeof(double*));
	for( int i = 0; i < M; i++)
		B[i] = (double*)calloc(M, sizeof(double));
	SistemaLinear sistemaLinear;

	int aux = 0;
	int numIteracoes = 0;
	for(;;){

		cout << "_______________________________________________________________________" << endl;
		cout << "Iteracao " << ++numIteracoes << endl;
	
		// Define matriz B
		aux = 0;
		for(list<int>::iterator l = listaB->begin(); l != listaB->end(); l++){
			for( int i = 0; i < M; i++)
				 B[i][aux] = A[i][*l];
			aux++;
		}

		// Define x
		for(int i = 0; i < N; i++)
			x[i] = 0;
		sistemaLinear.resolve(B,q,b,M,0);
		aux = 0;
		for(list<int>::iterator l = listaB->begin(); l != listaB->end(); l++)
			 x[*l] = q[aux++];

		// Define y
		aux = 0;
		for(list<int>::iterator l = listaB->begin(); l != listaB->end(); l++)
			p[aux++] = c[*l];
		sistemaLinear.resolve(B,y,p,M,1);

		// Define s
		for(int i = 0; i < N; i++)
			s[i] = 0;
		for(list<int>::iterator l = listaN->begin(); l != listaN->end(); l++){
			s[*l] = c[*l];
			for(int i = 0; i < M; i++)
				s[*l] -= y[i]*A[i][*l];
		}

		imprimeA(listaN,listaB,B,x,y,s,M,N);

		// Entrada na base
		int h = 0;
		while(h < N & s[h] >= 0)
			h++;

		// Calculando d
		for(int i = 0; i < M; i++)
			a[i] = A[i][h];
		sistemaLinear.resolve(B,d,a,M,0);
		cout << "Vetor d:";
		imprimeVetor(d,M);

		aux = 0;
		while(aux < N & s[aux++] >= 0);
		if(aux > N){
			double z = 0;
			for(int i = 0; i < N; i++)
				z += x[i]*c[i];
			mensagemSol(x,z,N);
			break;
		}

		aux = 0;
		while(aux < M & d[aux++] <= 0);
		if(aux > M){
			cout << "Ilimitado" << endl;
			break;
		}

		// Saida da base
		double min = 1e12;
		int k = 0;
		aux = 0;	
		for(list<int>::iterator l = listaB->begin(); l != listaB->end(); l++){
			if(d[aux] > 0 & x[*l]/d[aux] < min){
				k = *l;
				min = x[*l]/d[aux];
			}
			aux++;
		}

		// Atualizacao das listas
		listaB->push_back(h);
		listaN->remove(h);
		listaB->remove(k);
		listaN->push_back(k);
		cout << "h: " << h << " k: " << k << endl;
		
	}
	free(p);
	free(q);
	free(d);
	free(s);
	free(y);
	free(a);
	for(int i = 0; i < M; i++)
		free(B[i]);
	free(B);

	return 0;
}

int simplexDual(int M,int N,double** A,double* b,double* c,double* x,list<int> *listaN,list<int> *listaB){

	cout << "||===================================================================||" << endl;
	cout << "|| Algoritmo Simplex Dual                                            ||" << endl;
	cout << "||===================================================================||" << endl;
	
	double* p	= (double*)calloc(M,sizeof(double));
	double* q	= (double*)calloc(M,sizeof(double));
	double* s	= (double*)calloc(N,sizeof(double));
	double* y	= (double*)calloc(M,sizeof(double));
	double* d	= (double*)calloc(N-M,sizeof(double));
	double* e	= (double*)calloc(M,sizeof(double));
	double* zi	= (double*)calloc(M,sizeof(double));
	double** B	= (double**)calloc(M, sizeof(double*));
	for(int i = 0; i < M; i++)
		B[i] = (double*)calloc(M, sizeof(double));
	SistemaLinear sistemaLinear;

	int aux = 0;
	int numIteracoes = 0;
	for(;;){

		cout << "_______________________________________________________________________" << endl;
		cout << "Iteracao " << ++numIteracoes << endl;
	
		// Define matriz B
		aux = 0;
		for(list<int>::iterator l = listaB->begin(); l != listaB->end(); l++){
			for( int i = 0; i < M; i++)
				 B[i][aux] = A[i][*l];
			aux++;
		}

		// Define x
		for(int i = 0; i < N; i++)
			x[i] = 0;
		sistemaLinear.resolve(B,q,b,M,0);
		aux = 0;
		for(list<int>::iterator l = listaB->begin(); l != listaB->end(); l++)
			 x[*l] = q[aux++];

		// Define y
		aux = 0;
		for(list<int>::iterator l = listaB->begin(); l != listaB->end(); l++)
			p[aux++] = c[*l];
		sistemaLinear.resolve(B,y,p,M,1);

		// Define s
		for(int i = 0; i < N; i++)
			s[i] = 0;
		for(list<int>::iterator l = listaN->begin(); l != listaN->end(); l++){
			s[*l] = c[*l];
			for(int i = 0; i < M; i++)
				s[*l] -= y[i]*A[i][*l];
		}

		imprimeA(listaN,listaB,B,x,y,s,M,N);

		// Checa sol otima
		aux = 0;
		while(aux < N & x[aux++] >= 0);
		if(aux > N){
			double z = 0;
			for(int i = 0; i < N; i++)
				z += x[i]*c[i];
			mensagemSol(x,z,N);
			break;
		}

		// Saida da base
		int k	= 0;
		int r	= 0;
		aux		= 0;
		for(list<int>::iterator l = listaB->begin(); l != listaB->end(); l++){
			if(x[*l] < x[k]){
				k	= *l;
				r	= aux;
			}	
			aux++;
		}

		// Calculando z
		for(int i = 0; i < M; i++)
			if(i == r)
				e[i] = 1;
			else
				e[i] = 0;
		sistemaLinear.resolve(B,zi,e,M,1);
		cout << "Vetor zi:";
		imprimeVetor(zi,M);

		// Calculando d
		aux = 0;
		for(list<int>::iterator l = listaN->begin(); l != listaN->end(); l++){
			d[aux] = 0;
			for(int i = 0; i < M; i++)
				d[aux] += A[i][*l]*zi[i];
			aux++;
		}
		cout << "Vetor d:";
		imprimeVetor(d,N-M);

		aux = 0;
		while(aux < M & d[aux++] >= 0);
		if(aux > M){
			cout << "Ilimitado" << endl;
			break;
		}

		// Entrada na base
		double min = 1e12;
		int h = 0;
		aux = 0;	
		for(list<int>::iterator l = listaN->begin(); l != listaN->end(); l++){
			if(d[aux] < 0 & -s[*l]/d[aux] < min){
				h = *l;
				min = -s[*l]/d[aux];
			}
			aux++;
		}

		// Atualizacao das listas
		listaB->push_back(h);
		listaN->remove(h);
		listaB->remove(k);
		listaN->push_back(k);
		cout << "r: " << r+1 << " h: " << h+1 << " k: " << k+1 << endl;
		
	}
	free(p);
	free(q);
	free(s);
	free(y);
	free(d);
	free(e);
	free(zi);
	for(int i = 0; i < M; i++)
		free(B[i]);
	free(B);

	return 0;
}

int garfinkelNemhauser(int M,int N,double** A,double* b,double* c,double* x,list<int> *listaN,list<int> *listaB){

	cout << "||===================================================================||" << endl;
	cout << "|| Algoritmo Garfinkel-Nemhauser                                     ||" << endl;
	cout << "||===================================================================||" << endl;
	
	int sol	= 0;
	int s	= 0;
	int e	= 0;
	int aux = 0;
	int auxBase = 0;
	int numIteracoes = 0;
	double pivo = 0;
	int* vN	= (int*)calloc(N-M, sizeof(int));
	int* vB	= (int*)calloc(M, sizeof(int));
	double** Q	= (double**)calloc(M+1, sizeof(double*));
	for(int i = 0; i < M+1; i++)
		Q[i] = (double*)calloc(N-M+1, sizeof(double));

	// Define matriz Q
	aux = 0;
	Q[0][0] = 0;
	for(list<int>::iterator l = listaB->begin(); l != listaB->end(); l++)
		vB[aux++] = 1+*l;
	aux = 0;
	for(list<int>::iterator l = listaN->begin(); l != listaN->end(); l++){
		vN[aux] = 1+*l;
		Q[0][aux+1] = c[*l];
		for(int i = 0; i < M; i++)
			 Q[i+1][aux+1] = -A[i][*l];
		aux++;
	}
	for(int i = 0; i < M; i++)
		 Q[i+1][0] = -b[i];

	for(;;){

		cout << "_______________________________________________________________________" << endl;
		imprimeQuadroGH(++numIteracoes,Q,vB,vN,M,N-M);

		// Checa sol otima / Saida da base
		s = 0;
		while(++s <= M && Q[s][0] >= 0);
		if(s > M){
			sol = 1;
			break;
		}
		auxBase = vB[s-1];

		// Checa factibilidade
		e = 1;
		while(e <= N-M & Q[s][e] >= 0)
			e++;
		if(e >= N-M)
			break;

		// Entrada na base
		double min = 1e12;
		for(int i = 1; i < N-M+1; i++)
			if(Q[s][i] < 0 & -Q[0][i]/Q[s][i] < min){
				min = -Q[0][i]/Q[s][i];
				e = i;
			}
		pivo = Q[s][e];
		vB[s-1] = vN[e-1];
		vN[e-1] = auxBase;
		
		// AtualizaÃ§Ã£o do quadro
		for(int i = 0; i < M+1; i++)

			Q[i][e] /= (2*(i==s)-1)*pivo;		
		for(int i = 0; i < M+1; i++)
			if(i != s)
				for(int j = 0; j < M+1; j++)
					if(j != e)
						Q[i][j] += Q[i][e]*Q[s][j];		
		for(int j = 0; j < M+1; j++)
			if(j != e)
				Q[s][j] /= pivo;

	}

	if(sol){
		for(int i = 0; i < N; i++)
			x[0] = 0;
		aux = 0;
		for(int i = 0; i < M; i++)
			x[vB[i]-1] = Q[1+aux++][0];
		mensagemSol(x,-Q[0][0],N);
	}else{
		cout << "||===================================================================||" << endl;
		cout << "|| InfactÃ­vel                                                        ||" << endl;
		cout << "||===================================================================||" << endl;
	}

	for(int i = 0; i < M+1; i++)
		free(Q[i]);
	free(Q);

	return 0;
}

int afimEscala(int M,int N,double** A,double* b,double* c,double* x){

	cout << "||===================================================================||" << endl;
	cout << "|| Algoritmo Afim-Escala                                             ||" << endl;
	cout << "||===================================================================||" << endl;

	// AlocaÃ§Ã£o de memÃ³ria
	double** Abt	= (double**)calloc(N, sizeof(double*));
	for(int i = 0; i < N; i++)
		Abt[i] = (double*)calloc(M, sizeof(double));

	double** Ab	= (double**)calloc(M, sizeof(double*));
	for(int i = 0; i < M; i++)
		Ab[i] = (double*)calloc(N, sizeof(double));

	double** A1	= (double**)calloc(M, sizeof(double*));
	for(int i = 0; i < M; i++)
		A1[i] = (double*)calloc(N, sizeof(double));

	double** PAb	= (double**)calloc(N, sizeof(double*));
	for(int i = 0; i < N; i++)
		PAb[i] = (double*)calloc(N, sizeof(double));

	double** QM1	= (double**)calloc(M, sizeof(double*));
	for(int i = 0; i < M; i++)
		QM1[i] = (double*)calloc(M, sizeof(double));

	double** QM2	= (double**)calloc(M, sizeof(double*));
	for(int i = 0; i < M; i++)
		QM2[i] = (double*)calloc(M, sizeof(double));

	double** QN	= (double**)calloc(N, sizeof(double*));
	for(int i = 0; i < N; i++)
		QN[i] = (double*)calloc(N, sizeof(double));

	double* cb	= (double*)calloc(N, sizeof(double*));
	double* db	= (double*)calloc(N, sizeof(double*));
	double* d	= (double*)calloc(N, sizeof(double*));

	// Valores iniciais
	double espilon	= 1e-6;
	double alfa		= 0.995;
	double dif		= 1e12;
	double z		= 0;
	double custoAnt	= 0;
	do{

		cout << "Vetor x: ";
		imprimeVetor(x,N);

		z = 0;
		for(int i = 0; i < N; i++)
			z += c[i]*x[i];
		cout << "z: " << z << endl;

		// Prepara matrizes e vetores:
		double** X	= (double**)calloc(N, sizeof(double*));
		for(int i = 0; i < N; i++)
			X[i] = (double*)calloc(N, sizeof(double));
		for(int i = 0; i < N; i++)
			for(int j = 0; j < N; j++)
				if(i == j)
					X[i][j] = x[i];
				else
					X[i][j] = 0;

		// 1. MudanÃ§a de escala:
		mutiplica(A,X,Ab,M,N,N);
		mutiplica(X,c,cb,N,N);
		
		// 2. ProjeÃ§Ã£o:
		transposta(Ab,Abt,M,N);
		mutiplica(Ab,Abt,QM1,M,M,N);
		inversa(QM1,QM2,M);
		mutiplica(QM2,Ab,A1,M,N,M);
		mutiplica(Abt,A1,QN,N,N,M);
		identidade(PAb,N);
		soma(PAb,QN,PAb,-1,N,N);

		// 3. DireÃ§Ã£o de busca:
		mutiplica(PAb,cb,db,N,N);
		mutiplica(db,db,-1,N);

		// 4. Retorno Ã  escala original:
		mutiplica(X,db,d,N,N);

		// 5. Teste da razÃ£o:
		double lambda = 1e12;
		for(int j = 0; j < N; j++)
			if(d[j] < 0 & -x[j]/d[j] < lambda)
				lambda = -x[j]/d[j];
		soma(x,d,x,alfa*lambda,N);

		custoAnt = z;
		z = 0;
		for(int i = 0; i < N; i++)
			z += c[i]*x[i];
		dif = abs(z - custoAnt);

	}while(dif >= espilon);
	mensagemSol(x,z,N);

	for(int i = 0; i < N; i++)
		free(Abt[i]);
	free(Abt);
	for(int i = 0; i < M; i++)
		free(Ab[i]);
	free(Ab);
	for(int i = 0; i < M; i++)
		free(A1[i]);
	free(A1);
	for(int i = 0; i < N; i++)
		free(PAb[i]);
	free(PAb);
	for(int i = 0; i < M; i++)
		free(QM1[i]);
	free(QM1);
	for(int i = 0; i < M; i++)
		free(QM2[i]);
	free(QM2);
	for(int i = 0; i < N; i++)
		free(QN[i]);
	free(QN);
	free(cb);
	free(db);
	free(d);

	return 0;
}

int karmarkar(int M,int N,double** A,double* b,double* c,double* x){

	cout << "||===================================================================||" << endl;
	cout << "|| Algoritmo Karmarkar                                               ||" << endl;
	cout << "||===================================================================||" << endl;

	// AlocaÃ§Ã£o de memÃ³ria
	double** Abt	= (double**)calloc(N, sizeof(double*));
	for(int i = 0; i < N; i++)
		Abt[i] = (double*)calloc(M+1, sizeof(double));

	double** Ab	= (double**)calloc(M+1, sizeof(double*));
	for(int i = 0; i < M+1; i++)
		Ab[i] = (double*)calloc(N, sizeof(double));

	double** A1	= (double**)calloc(M+1, sizeof(double*));
	for(int i = 0; i < M+1; i++)
		A1[i] = (double*)calloc(N, sizeof(double));

	double** P	= (double**)calloc(N, sizeof(double*));
	for(int i = 0; i < N; i++)
		P[i] = (double*)calloc(N, sizeof(double));

	double** QM1	= (double**)calloc(M+1, sizeof(double*));
	for(int i = 0; i < M+1; i++)
		QM1[i] = (double*)calloc(M+1, sizeof(double));

	double** QM2	= (double**)calloc(M+1, sizeof(double*));
	for(int i = 0; i < M+1; i++)
		QM2[i] = (double*)calloc(M+1, sizeof(double));

	double** QN	= (double**)calloc(N, sizeof(double*));
	for(int i = 0; i < N; i++)
		QN[i] = (double*)calloc(N, sizeof(double));

	double* cb	= (double*)calloc(N, sizeof(double*));
	double* cp	= (double*)calloc(N, sizeof(double*));
	double* d	= (double*)calloc(N, sizeof(double*));
	double* y	= (double*)calloc(N, sizeof(double*));
	double* y0	= (double*)calloc(N, sizeof(double*));

	// Valores iniciais
	for(int i = 0; i < N; i++){
		x[i]	= (double)1/N;
		y[i]	= (double)1/N;
		y0[i]	= (double)1/N;
	}
	double r		= 1/sqrt(N*(N-1));
	double alfa		= (double)(N-1)/(3*N);
	double z	= 0;
	double epsilon	= 1e-6;
	double L = -1e12;
	for(int i = 0; i < N; i++)
		if(c[i] > L)
			L = c[i];
	L = ceil(1+log(1+(double)abs(L))+log((double)abs(determinante(A,M))));
	cout << "L:"		<<  setprecision(9) << setw(12) << pow(2,-L) << endl;
	cout << "epsilon:"	<<  setprecision(9) << setw(12) << epsilon << endl;
	do{

		// Matriz diagonal:
		double** X	= (double**)calloc(N, sizeof(double*));
		for(int i = 0; i < N; i++)
			X[i] = (double*)calloc(N, sizeof(double));
		for(int i = 0; i < N; i++)
			for(int j = 0; j < N; j++)
				if(i == j)
					X[i][j] = x[i];
				else
					X[i][j] = 0;

		// MudanÃ§a de escala:
		mutiplica(X,c,cb,N,N);
		mutiplica(A,X,Ab,M,N,N);
		for(int i = 0; i < N; i++)
			Ab[M][i] = 1;
		
		// Matriz de projeÃ§Ã£o:
		transposta(Ab,Abt,M+1,N);
		mutiplica(Ab,Abt,QM1,M+1,M+1,N);
		inversa(QM1,QM2,M+1);
		mutiplica(QM2,Ab,A1,M+1,N,M+1);
		mutiplica(Abt,A1,QN,N,N,M+1);
		identidade(P,N);
		soma(P,QN,P,-1,N,N);

		// ProjeÃ§Ã£o do o z no nulo de Ab:
		mutiplica(P,cb,cp,N,N);

		// Caminhada rumo a uma nova soluÃ§Ã£o:
		float dba = 0;
		for(int i = 0; i < N; i++)
			dba += pow(cp[i],2);
		dba = sqrt(dba);
		soma(y0,cp,y,-alfa*r/dba,N);

		// Determina nova soluÃ§Ã£o:
		mutiplica(X,y,x,N,N);
		double a = 0;
		for(int i = 0; i < N; i++)
			a += x[i];
		mutiplica(x,x,1/a,N);

		// CÃ¡lculo do z:
		z = 0;
		for(int i = 0; i < N; i++)
			z += c[i]*x[i];
		cout << "z:" <<  setprecision(9) << setw(12) <<  z << endl;
	}while(z >= epsilon);

	mensagemSol(x,z,N);

	for(int i = 0; i < N; i++)
		free(Abt[i]);
	free(Abt);
	for(int i = 0; i < M+1; i++)
		free(Ab[i]);
	free(Ab);
	for(int i = 0; i < M+1; i++)
		free(A1[i]);
	free(A1);
	for(int i = 0; i < N; i++)
		free(P[i]);
	free(P);
	for(int i = 0; i < M+1; i++)
		free(QM1[i]);
	free(QM1);
	for(int i = 0; i < M+1; i++)
		free(QM2[i]);
	free(QM2);
	for(int i = 0; i < N; i++)
		free(QN[i]);
	free(QN);
	free(cb);
	free(cp);
	free(d);
	free(y);
	free(y0);
	return 0;
}

int primalDual(int M,int N,double** A,double* b,double* c,double* x,double* s,double* y){

	cout << "||===================================================================||" << endl;
	cout << "|| Algoritmo Primal-Dual                                             ||" << endl;
	cout << "||===================================================================||" << endl;

	// AlocaÃ§Ã£o de memÃ³ria
	double* cdx	= (double*)calloc(N,sizeof(double));
	double* pds	= (double*)calloc(N,sizeof(double));
	double* up 	= (double*)calloc(N,sizeof(double));
	double* xs 	= (double*)calloc(N,sizeof(double));
	double* u 	= (double*)calloc(N,sizeof(double));
	double* v 	= (double*)calloc(N,sizeof(double));
	double* p 	= (double*)calloc(N,sizeof(double));
	double* D 	= (double*)calloc(N,sizeof(double));
	double* ds 	= (double*)calloc(N,sizeof(double));
	double* dx 	= (double*)calloc(N,sizeof(double));
	double* t 	= (double*)calloc(M,sizeof(double));
	double* vM 	= (double*)calloc(M,sizeof(double));
	double* dy 	= (double*)calloc(M,sizeof(double));

	double** AD	= (double**)calloc(M, sizeof(double*));
	for(int i = 0; i < M; i++)
		AD[i] = (double*)calloc(N, sizeof(double));

	double** At	= (double**)calloc(N, sizeof(double*));
	for(int i = 0; i < N; i++)
		At[i] = (double*)calloc(M, sizeof(double));

	double** ADA	= (double**)calloc(M, sizeof(double*));
	for(int i = 0; i < M; i++)
		ADA[i] = (double*)calloc(M, sizeof(double));

	double** ADAi = (double**)calloc(M, sizeof(double*));
	for(int i = 0; i < M; i++)
		ADAi[i] = (double*)calloc(M, sizeof(double));

	double mi = 0;
	double z = 0;

	double E1 = 5e-2;
	double E2 = 1e-6;
	double E3 = 1e-6;

	double e2 = 0;
	double e3 = 0;

	double a = 0.99;
	double BP = 0;
	double BD = 0;
	double maxTemp = 0;

	int sol = 0;
	int aux = 0;

	cout << "Vetor x: ";
	imprimeVetor(x,N);

	cout << "Vetor s: ";
	imprimeVetor(s,N);

	cout << "Vetor y: ";
	imprimeVetor(y,M);

	int numIteracoes = 0;
	while(1){

		cout << "_______________________________________________________________________" << endl;
		cout << "Iteracao " << ++numIteracoes << endl;
		mi = produtoEscalar(x,s,N)/N;

		// ------------------------------------
		// CÃ¡lculos intermediÃ¡rios
		// ------------------------------------

		// t = b - Ax
		mutiplica(A,x,t,M,N);
		soma(b,t,t,-1,M);

		// u = c-A'y - s
		transposta(A,At,M,N);
		mutiplica(At,y,u,N,M);
		soma(c,u,u,-1,N);
		soma(u,s,u,-1,N);

		// v = ue - xse
		for(int j = 0; j < N; j++)
			v[j] = mi - x[j]*s[j];

		// p = v/x
		for(int j = 0; j < N; j++)
			p[j] = v[j]/x[j];

		// D = x/s
		for(int j = 0; j < N; j++)
			D[j] = x[j]/s[j];

		z = 0;
		for(int j = 0; j < N; j++)
			z -= x[j]*c[j];
		cout << "z: " << z << endl;

		// ------------------------------------


		// ------------------------------------
		// Teste de otimalidade	
		// ------------------------------------
		e2 = modulo(t,M)/(modulo(b,M)+1);
		e3 = modulo(u,N)/(modulo(c,N)+1);
		cout << "mi: " << mi << " e2: " << e2 << " e3: " << e3 << endl;
		if(mi < E1 && e2 < E2 && e3 < E3)
			break;
		// ------------------------------------


		// ------------------------------------
		// CÃ¡lculo das direÃ§Ãµes de translaÃ§Ã£o
		// ------------------------------------

		// AD
		for(int i = 0; i < M; i++)
			for(int j = 0; j < N; j++)
				AD[i][j] = A[i][j]*D[j];
		
		// (A*D*A')-1
		mutiplica(AD,At,ADA,M,M,N);
		inversa(ADA,ADAi,M);
		
		// AD(u-p) + t
		soma(u,p,up,-1,N);
		mutiplica(AD,up,vM,M,N);
		soma(vM,t,vM,1,M);

		// dy
		mutiplica(ADAi,vM,dy,M,M);

		// ds
		mutiplica(At,dy,ds,N,M);
		soma(u,ds,ds,-1,N);
			
		// dx
		soma(p,ds,pds,-1,N);
		for(int j = 0; j < N; j++)
			dx[j] = D[j]*pds[j];

		cout << "Vetor dx: ";
		imprimeVetor(dx,N);

		cout << "Vetor dy: ";
		imprimeVetor(dy,M);

		cout << "Vetor ds: ";
		imprimeVetor(ds,N);

		// ------------------------------------


		// ------------------------------------
		// VerificaÃ§Ã£o de soluÃ§Ã£o ilimitada
		// ------------------------------------

		// Primal Ilimitado
		if(igualZero(t,M,1e-6) && maiorQueZero(dx,N) && produtoEscalar(c,dx,N) < 0){
			sol = 1;
			break;
		}

		// Dual Ilimitado
		if(igualZero(u,N,1e-6) && maiorQueZero(ds,N) && produtoEscalar(b,dy,M) > 0){
			sol = 2;
			break;
		}


		// ------------------------------------
		// Comprimento dos passos
		// ------------------------------------

		BP = 1;
		maxTemp = 0;
		for(int j = 0; j < N; j++){
			maxTemp = -dx[j]/(a*x[j]);
			if(maxTemp > BP)
				BP = maxTemp;
		}
		BP = 1/BP;
		BD = 1;
		maxTemp = 0;
		for(int j = 0; j < N; j++){
			maxTemp = -ds[j]/(a*s[j]);
			if(maxTemp > BD)
				BD = maxTemp;
		}
		BD = 1/BD;

		cout << "BP: " << BP << " BD: " << BD << endl;

		// ------------------------------------


		// ------------------------------------
		// Movendo para um novo ponto
		// ------------------------------------

		cout << "Vetor x: ";
		imprimeVetor(x,N);
		
		cout << "Vetor s: ";
		imprimeVetor(s,N);
		
		soma(x,dx,x,BP,N);
		soma(y,dy,y,BD,M);
		soma(s,ds,s,BD,N);

		// ------------------------------------

	}

	switch(sol){
		case 1:
			cout << "Primal Ilimitado" << endl;
			break;
		case 2:
			cout << "Dual Ilimitado" << endl;
			break;
		default:
			mensagemSolPrimalDual(x,s,z,N);
			break;
	}

	for(int i = 0; i < M; i++)
		free(AD[i]);
	free(AD);

	for(int i = 0; i < N; i++)
		free(At[i]);
	free(At);
	
	free(cdx);
	free(pds);
	free(up);
	free(xs);
	free(u);
	free(v);
	free(p);
	free(D);
	free(ds);
	free(dx);
	free(t);
	free(vM);
	free(dy);
}