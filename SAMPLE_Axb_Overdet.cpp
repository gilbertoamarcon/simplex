#include <iostream>
#include <iomanip>
#include <list>
#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include "sistemaLinear.hpp"

using namespace std;

// Solving an overdetermined system Ax = b
// for A mxn, x nx1 and b mx1, m >= n

int main(int argc, char **argv){

	int M = 3; 
	int N = 2;

	double** A;		// mxn
	double** At;	// nxm
	double** AtA;	// nxn
	double** AtAi;	// nxn
	double** Ap;	// nxm
	double* x;		// nx1
	double* b;		// mx1
	
	A = (double**)calloc(M, sizeof(double*));
	for(int i = 0; i < M; i++)
		A[i] = (double*)calloc(N, sizeof(double));
	
	At = (double**)calloc(N, sizeof(double*));
	for(int i = 0; i < N; i++)
		At[i] = (double*)calloc(M, sizeof(double));
	
	AtA = (double**)calloc(N, sizeof(double*));
	for(int i = 0; i < N; i++)
		AtA[i] = (double*)calloc(N, sizeof(double));
	
	AtAi = (double**)calloc(N, sizeof(double*));
	for(int i = 0; i < N; i++)
		AtAi[i] = (double*)calloc(N, sizeof(double));
	
	Ap = (double**)calloc(N, sizeof(double*));
	for(int i = 0; i < N; i++)
		Ap[i] = (double*)calloc(M, sizeof(double));

	x = (double*)calloc(N,sizeof(double));
	b = (double*)calloc(M,sizeof(double));

	A[0][0] = 1;
	A[0][1] = 1;
	A[1][0] = 2;
	A[1][1] = 1;
	A[2][0] = 3;
	A[2][1] = 1;

	b[0] =  0.0;
	b[1] = -2.0;
	b[2] = -4.0;

	transposta(A,At,M,N);
	mutiplica(At,A,AtA,N,N,M);
	inversa(AtA,AtAi,N);
	mutiplica(AtAi,At,Ap,N,M,N);
	mutiplica(Ap,b,x,N,M);

	cout << "Matriz A:" << endl;
	imprimeMatriz(A,M,N);

	cout << "Vetor x:";
	imprimeVetor(x,N);

	cout << "Vetor b:";
	imprimeVetor(b,M);

	cout << "Matriz At:" << endl;
	imprimeMatriz(At,N,M);

	cout << "Matriz Ap:" << endl;
	imprimeMatriz(Ap,N,M);

	for(int i = 0; i < M; i++)
		free(A[i]);
	free(A);

	for(int i = 0; i < N; i++)
		free(At[i]);
	free(At);

	for(int i = 0; i < N; i++)
		free(AtA[i]);
	free(AtA);

	for(int i = 0; i < N; i++)
		free(AtAi[i]);
	free(AtAi);

	for(int i = 0; i < N; i++)
		free(Ap[i]);
	free(Ap);

	free(x);
	free(b);

	return 0;
}